﻿function unzip ($zip, $destination) {
	Add-Type -Path ((Get-Location).Path + '\lib\ICSharpCode.SharpZipLib.dll')
	$fs = New-Object ICSharpCode.SharpZipLib.Zip.FastZip
	$fs.ExtractZip($zip, $destination, '')
}

function download_from_storage ($container, $blob, $connection, $destination) {
    Add-Type -Path ((Get-Location).Path + '\Microsoft.WindowsAzure.StorageClient.dll')
    $storageAccount = [Microsoft.WindowsAzure.CloudStorageAccount]::Parse($connection)
    $blobClient = New-Object Microsoft.WindowsAzure.StorageClient.CloudBlobClient($storageAccount.BlobEndpoint, $storageAccount.Credentials)   
    $remoteBlob = $blobClient.GetBlobReference($container + '/' + $blob)
    $remoteBlob.DownloadToFile($destination + "\" + $blob)
}

$connection_string = 'DefaultEndpointsProtocol=http;AccountName=<your.storage.account.name>;AccountKey=<your.storage.account.key>'

# DIST
$dist = 'dist.zip'
download_from_storage 'package' $dist $connection_string (Get-Location).Path
unzip ((Get-Location).Path + "\" + $dist) (Get-Location).Path

# Launch Amdatu
cd .\amdatu
cmd /c .\run.bat